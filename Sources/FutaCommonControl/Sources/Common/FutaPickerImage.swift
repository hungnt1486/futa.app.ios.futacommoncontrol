//  File name   : PickerImage.swift
//
//  Author      : Dung Vu
//  Created date: 3/12/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import RxSwift
import RxCocoa
import Photos
import FwiCore

public enum FutaPickerImageError: Error {
    case permission(type: UIImagePickerController.SourceType)
    case noPhoto
    case custom(error: Error)
}

public typealias FutaPickerImagePhotoDelegate = (UIImagePickerControllerDelegate & UINavigationControllerDelegate)
public typealias FutaImagePickerResult = Result<UIImage, FutaPickerImageError>
public typealias FutaPickerImageBlockResult = ([UIImagePickerController.InfoKey : Any]) -> FutaImagePickerResult

public final class FutaPickerImage: NSObject {
    /// Class's public properties.
    private weak var controller: UIViewController?
    private var allowEdit: Bool = false
    private var block: FutaPickerImageBlockResult?
    private lazy var disposeBag = DisposeBag()
    
    @Publisher private var mImage: FutaImagePickerResult
    var image: Observable<FutaImagePickerResult> {
        return $mImage.observeOn(MainScheduler.asyncInstance)
    }
    
    public init(with controller: UIViewController?) {
        self.controller = controller
        super.init()
    }
    
    private func request(permission type: UIImagePickerController.SourceType) -> Observable<Result<Bool, FutaPickerImageError>> {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return Observable.just(.failure(.permission(type: type)))
        }
        
        return Observable.create { (s) -> Disposable in
            let showError: () -> () = {
                s.onNext(.failure(.permission(type: type)))
                s.onCompleted()
            }
            
            let completed: (Bool) -> () = {
                s.onNext(.success($0))
                s.onCompleted()
            }
            
            switch type {
            case .camera:
                let status = AVCaptureDevice.authorizationStatus(for: .video)
                switch status {
                case .authorized:
                    completed(true)
                case .notDetermined:
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (grant) in
                        if grant {
                            completed(true)
                        } else {
                            showError()
                        }
                    })
                default:
                    showError()
                }
            case .photoLibrary, .savedPhotosAlbum:
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                case .authorized:
                    completed(true)
                case .notDetermined:
                    PHPhotoLibrary.requestAuthorization { (t) in
                        switch t {
                        case .authorized:
                            completed(true)
                        default:
                            showError()
                        }
                    }
                default:
                    showError()
                }
            @unknown default:
                fatalError("No Process for this case")
            }
            return Disposables.create()
        }
    }
    
    
    /// Show picker to choose
    /// - Parameters:
    ///   - source: camera, photo
    ///   - allowEdit: edit photo
    ///   - block: custom result return
    public func showPicker(source: UIImagePickerController.SourceType,
                    allowEdit: Bool,
                    block: FutaPickerImageBlockResult? = nil)
    {
        self.block = block
        self.allowEdit = allowEdit
        request(permission: source).observeOn(MainScheduler.asyncInstance).subscribe(onNext: weakify({ (result, wSelf) in
            switch result {
            case .success:
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = wSelf
                imagePicker.sourceType = source
                imagePicker.allowsEditing = allowEdit
                wSelf.controller?.present(imagePicker, animated: true, completion: nil)
            case .failure(let e):
                wSelf.mImage = .failure(e)
            }
        })).disposed(by: disposeBag)
    }

    /// Class's private properties.
}
 
// MARK: Class's public methods
extension FutaPickerImage: FutaPickerImagePhotoDelegate, Weakifiable {
    private func handler(result: [UIImagePickerController.InfoKey : Any]) {
        let image: FutaPickerImageBlockResult = block ?? { [unowned self] in
            let key: UIImagePickerController.InfoKey = self.allowEdit ? .editedImage : .originalImage
            if let r = $0[key] as? UIImage {
                return .success(r)
            } else {
                return .failure(.noPhoto)
            }
        }
        mImage = image(result)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: weakify({ (wSelf) in
            wSelf.handler(result: info)
        }))
    }

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: -- Protocol Pick Image
public protocol PickImageCameraAndPhotoProtocol {
    var controller: UIViewController { get }
}

private struct FutaImagePickerKey {
    static var name = "ImagePickerKey"
}

public extension PickImageCameraAndPhotoProtocol {
    private var imagePicker: FutaPickerImage! {
        get {
            guard let r = objc_getAssociatedObject(self, &FutaImagePickerKey.name) as? FutaPickerImage else {
                let new = FutaPickerImage(with: controller)
                defer {
                    objc_setAssociatedObject(self, &FutaImagePickerKey.name, new, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                }
                return new
            }
            return r
        }
        
        set {
            objc_setAssociatedObject(self, &FutaImagePickerKey.name, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var image: Observable<FutaImagePickerResult> {
        return imagePicker.image
    }
    
    /// Show picker to choose
    /// - Parameters:
    ///   - source: camera, photo
    ///   - allowEdit: edit photo
    ///   - block: custom result return
    func showPicker(source: UIImagePickerController.SourceType,
                    allowEdit: Bool,
                    block: FutaPickerImageBlockResult? = nil) {
        imagePicker.showPicker(source: source, allowEdit: allowEdit, block: block)
    }
}
