//  File name   : HeaderCornerView.swift
//
//  Author      : Dung Vu
//  Created date: 1/11/19
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2019 Vato. All rights reserved.
//  --------------------------------------------------------------

import UIKit

public final class FutaHeaderCornerView: UIView {
    /// Class's public properties.
    private let radius: CGFloat
    private let shapeLayer: CAShapeLayer = CAShapeLayer()
    public var corners: UIRectCorner = [.topLeft, .topRight]
    public var cornersFooter: UIRectCorner = [.bottomLeft, .bottomRight]
    public var isUpdatedFooter: Bool = false
    @objc public var containerColor: UIColor? = .clear {
        didSet {
            shapeLayer.fillColor = containerColor?.cgColor
        }
    }
    
    private var currentRect: CGRect = .zero {
        didSet {
            guard currentRect != oldValue else {
                return
            }
            isUpdatedFooter ? footerCorners() : setupView()
        }
    }
    
    public override var frame: CGRect {
        get {
           return super.frame
        }
        
        set {
            super.frame = newValue
            setNeedsDisplay()
        }
    }
    
    @objc public init(with cornerRadius: CGFloat) {
        radius = cornerRadius
        super.init(frame: .zero)
        self.backgroundColor = .clear
    }
    
    @objc public init(withFooter cornerRadius: CGFloat) {
        radius = cornerRadius
        isUpdatedFooter = true
        super.init(frame: .zero)
        self.backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        shapeLayer.removeFromSuperlayer()
        shapeLayer.frame = currentRect
        let bezier = UIBezierPath(roundedRect: currentRect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius * 2, height: radius * 2))
        shapeLayer.path = bezier.cgPath
        shapeLayer.fillColor = containerColor?.cgColor
        self.layer.insertSublayer(shapeLayer, at: 0)
    }
    
    public func footerCorners() {
        shapeLayer.removeFromSuperlayer()
        shapeLayer.frame = currentRect
        let bezier = UIBezierPath(roundedRect: currentRect, byRoundingCorners: cornersFooter, cornerRadii: CGSize(width: radius * 2, height: radius * 2))
        shapeLayer.path = bezier.cgPath
        shapeLayer.fillColor = containerColor?.cgColor
        self.layer.insertSublayer(shapeLayer, at: 0)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.currentRect = rect
    }
    
    deinit {
        debugPrint("\(#function)")
    }
}
