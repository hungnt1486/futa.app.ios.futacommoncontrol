//  File name   : Error.swift
//
//  Author      : Dung Vu
//  Created date: 3/12/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public extension NSError {
    convenience init(use message: String?) {
        var userInfo = [String: Any]()
        userInfo[NSLocalizedDescriptionKey] = message
        self.init(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: userInfo)
    }
}
