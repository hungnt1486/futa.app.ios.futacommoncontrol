//  File name   : Operator.swift
//
//  Author      : Dung Vu
//  Created date: 9/14/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Vato. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import FwiCore

public typealias BlockModifyDirect<T> = (inout T) throws -> Void
public typealias BlockAdjustByValue<Value, T> = (Value) -> T

public protocol AdjustValueProtocol {
    static func +=(lhs: inout Self, block: BlockModifyDirect<Self>) rethrows
    static func >>>(lhs: Self, block: BlockModifyDirect<Self>) rethrows -> Self
}

public extension AdjustValueProtocol {
    static func +=(lhs: inout Self, block: BlockModifyDirect<Self>) rethrows {
        try block(&lhs)
    }
    
    static func >>>(lhs: Self, block: BlockModifyDirect<Self>) rethrows -> Self {
        var lhs = lhs
        try lhs += block
        return lhs
    }
}

extension UIEdgeInsets: AdjustValueProtocol {}
extension CGRect: AdjustValueProtocol {}
extension CGSize: AdjustValueProtocol {}

public extension UIEdgeInsets {
    static let topEdge: BlockAdjustByValue<CGFloat, UIEdgeInsets> = { UIEdgeInsets(top: $0, left: 0, bottom: 0, right: 0) }
    static let leftEdge: BlockAdjustByValue<CGFloat, UIEdgeInsets> = { UIEdgeInsets(top: 0, left: $0, bottom: 0, right: 0) }
    static let bottomEdge: BlockAdjustByValue<CGFloat, UIEdgeInsets> = { UIEdgeInsets(top: 0, left: 0, bottom: $0, right: 0) }
    static let rightEdge: BlockAdjustByValue<CGFloat, UIEdgeInsets> = { UIEdgeInsets(top: 0, left: 0, bottom: 0, right: $0) }
    
    static func +=(lhs: inout UIEdgeInsets, rhs: UIEdgeInsets) {
        lhs += {
            $0.top += rhs.top
            $0.left += rhs.left
            $0.bottom += rhs.bottom
            $0.right += rhs.right
        }
    }
}

public extension RawRepresentable where RawValue: Equatable {
    static func compare(value: RawValue?, conditions: Self...) -> Bool {
        guard let value = value else { return false }
        for i in conditions {
            if i.rawValue == value {
                return true
            }
        }
        return false
    }
}

public extension RawRepresentable {
    init?(optional value: RawValue?) {
        guard let value = value else {
            return nil
        }
        self.init(rawValue: value)
    }
}



