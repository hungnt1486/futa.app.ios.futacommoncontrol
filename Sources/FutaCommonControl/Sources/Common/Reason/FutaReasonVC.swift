//  File name   : FutaReasonVC.swift
//
//  Author      : Dung Vu
//  Created date: 3/11/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import FwiCore
import FwiCoreRX

// MARK: - Protocol
public protocol ReasonProtocol: CustomStringConvertible {
    var id: Int? { get }
    var urlImages: [URL]? { get }
}

public extension ReasonProtocol {
    var allowEditDescription: Bool {
        return id == -1
    }
    
    var urlImages: [URL]? { return nil }
}

// MARK: - Style
public typealias FutaReasonItemStyle<T> = (normal: T?, disabled: T?)

protocol ApplyUIProtocol {
    associatedtype UIType: UIView
    func apply(_ view: UIType)
}

enum ReasonInputType: Int {
    case begin
    case end
}

public struct FutaReasonButtonConfirmStyle: ApplyUIProtocol {
    public let text: FutaReasonItemStyle<NSAttributedString>
    public let background: FutaReasonItemStyle<UIImage>
    public let height: CGFloat
    
    /// radius: if nil -> round by height, else use this value
    public let radius: CGFloat?
    
    public init(text: FutaReasonItemStyle<NSAttributedString>,
                background: FutaReasonItemStyle<UIImage>,
                height: CGFloat,
                radius: CGFloat? = nil)
    {
        self.text = text
        self.background = background
        self.height = height
        self.radius = radius
    }
    
    func apply(_ view: UIButton) {
        view.setAttributedTitle(text.normal, for: .normal)
        view.setAttributedTitle(text.disabled, for: .disabled)
        view.setBackgroundImage(background.normal, for: .normal)
        view.setBackgroundImage(background.disabled, for: .disabled)
        view.layer.cornerRadius = radius ?? (height / 2)
        view.clipsToBounds = true
    }
}

public struct FutaReasonButtonCloseStyle: ApplyUIProtocol {
    public let text: NSAttributedString?
    public let icon: UIImage?
    public let size: CGSize
    public let contentEdges: UIEdgeInsets
    
    public init(text: NSAttributedString?, icon: UIImage?, size: CGSize, contentEdges: UIEdgeInsets = .zero) {
        self.text = text
        self.icon = icon
        self.size = size
        self.contentEdges = contentEdges
    }
    
    func apply(_ view: UIButton) {
        view.setAttributedTitle(text, for: .normal)
        view.setImage(icon, for: .normal)
        view.contentEdgeInsets = contentEdges
    }
}

public struct FutaRegisterCell<C: UITableViewCell> where C: UpdateDisplayProtocol {
    public let type: RegisterCellType
    public let adjustCell: BlockAction<(Int, C.Value, C)>?
    
    
    /// Create style register cell for table
    /// - Parameters:
    ///   - identifier: name identifier to register
    ///   - register: block use for register , it returns `tableView`, `Cell Type`, `Identifier`
    ///   - adjustCell: custom cell if needed
    public init(type: RegisterCellType = .class,
                adjustCell: BlockAction<(Int, C.Value, C)>? = nil) {
        self.type = type
        self.adjustCell = adjustCell
    }
    
    func registerCell(_ tableView: UITableView) {
        tableView.register(type: C.self, kind: type)
    }
}

public struct FutaTableViewStyle: ApplyUIProtocol {
    let style: UITableView.Style
    let separatorStyle: UITableViewCell.SeparatorStyle
    let separatorColor: UIColor?
    let rowHeight: CGFloat
    let estimateRowHeight: CGFloat
    let separatorInset: UIEdgeInsets
    
    public init(style: UITableView.Style = .plain,
                separatorStyle: UITableViewCell.SeparatorStyle = .none,
                separatorColor: UIColor? = nil,
                separatorInset: UIEdgeInsets = .zero,
                rowHeight: CGFloat = UITableView.automaticDimension,
                estimateRowHeight: CGFloat = 50)
    {
        self.style = style
        self.separatorStyle = separatorStyle
        self.separatorColor = separatorColor
        self.rowHeight = rowHeight
        self.estimateRowHeight = estimateRowHeight
        self.separatorInset = separatorInset
    }
    
    func generateTableView() -> UITableView {
        let tableView = UITableView(frame: .zero, style: style)
        apply(tableView)
        return tableView
    }
    
    func apply(_ view: UITableView) {
        view.separatorColor = separatorColor
        view.separatorStyle = separatorStyle
        view.rowHeight = rowHeight
        view.estimatedRowHeight = estimateRowHeight
        view.separatorInset = separatorInset
    }
}

public struct FutaReasonInputTextStyle: ApplyUIProtocol {
    public let placeHolder: NSAttributedString?
    public let heightInputText: CGFloat
    public let radius: CGFloat
    public let colorBorder: UIColor?
    public let borderWidth: CGFloat
    public let insetContainer: UIEdgeInsets
    public let insetContentInput: UIEdgeInsets
    public let tintColor: UIColor
    public let backgroundInputText: UIColor
    public let attributeText: [NSAttributedString.Key: Any]
    
    public init(placeHolder: NSAttributedString? = nil,
                heightInputText: CGFloat = 90,
                radius: CGFloat = 8,
                colorBorder: UIColor? = #colorLiteral(red: 0.3882352941, green: 0.4470588235, blue: 0.5019607843, alpha: 1),
                borderWidth: CGFloat = 1,
                backgroundInputText: UIColor = .white,
                tintColor: UIColor = .black,
                insetContainer: UIEdgeInsets = .init(top: 16, left: 16, bottom: 16, right: 16),
                insetContentInput: UIEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8),
                attributeText: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black,
                                                                .font: UIFont.systemFont(ofSize: 12, weight: .regular)]) {
        self.placeHolder = placeHolder
        self.heightInputText = heightInputText
        self.radius = radius
        self.backgroundInputText = backgroundInputText
        self.colorBorder = colorBorder
        self.borderWidth = borderWidth
        self.tintColor = tintColor
        self.insetContainer = insetContainer
        self.insetContentInput = insetContentInput
        self.attributeText = attributeText
    }
    
    func apply(_ view: UITextView) {
        view.clipsToBounds = true
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = colorBorder?.cgColor
        if #available(iOS 11, *) {
            view.contentInsetAdjustmentBehavior = .never
        }
        view.textContainerInset = insetContentInput
        view.layer.cornerRadius = radius
        view.tintColor = tintColor
        view.backgroundColor = backgroundInputText
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.placeHolderAttribute = placeHolder
    }
}

public struct FutaReasonVCStyle {
    let customize: BlockAction<UIViewController>
    public init(_ block: @escaping BlockAction<UIViewController>) {
        self.customize = block
    }
}

struct ReasonCustom: ReasonProtocol {
    var id: Int?
    var description: String
    var urlImages: [URL]? = nil
}


// MARK: - Main
public final class FutaReasonVC<C: UITableViewCell>: UIViewController where
                                                            C: UpdateDisplayProtocol, C.Value: ReasonProtocol
{
    public typealias Element = C.Value
    /// Class's public properties.
    @VariableReplay(wrappedValue: []) private var mSource: [Element]
    private let styleController: FutaReasonVCStyle
    private let styleButtonConfirm: FutaReasonButtonConfirmStyle
    private let styleButtonClose: FutaReasonButtonCloseStyle
    private let styleInputText: FutaReasonInputTextStyle
    private let styleCell: FutaRegisterCell<C>
    private let styleTable: FutaTableViewStyle
    private var source: Observable<[Element]>
    @VariableReplay private var currentSelected: IndexPath?
    
    private lazy var btnConfirm: UIButton = .init(type: .custom)
    private lazy var btnClose: UIButton = .init(type: .custom)
    private lazy var textView: UITextView = .init(frame: .zero)
    private lazy var containerInput: UIView = .init(frame: .zero)
    private lazy var tableView = styleTable.generateTableView()
    private lazy var disposeBag = DisposeBag()
    private lazy var cachedContainerInputSize: [ReasonInputType: CGSize] = [:]
    private var cachedRectTextView: CGRect?
    private let reasonInputImageProtocol: ReasonInputImageProtocol?
    @Publisher var selected: ReasonProtocol?
    
    init(styleController: FutaReasonVCStyle,
         cell: FutaRegisterCell<C>,
         styleTable: FutaTableViewStyle,
         buttonClose: FutaReasonButtonCloseStyle,
         buttonConfirm: FutaReasonButtonConfirmStyle,
         styleInputText: FutaReasonInputTextStyle,
         reasonInputImageProtocol: ReasonInputImageProtocol?,
         source: Observable<[Element]>)
    {
        self.styleController = styleController
        self.styleTable = styleTable
        self.styleButtonClose = buttonClose
        self.styleInputText = styleInputText
        self.source = source
        self.styleButtonConfirm = buttonConfirm
        self.styleCell = cell
        self.reasonInputImageProtocol = reasonInputImageProtocol
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    // MARK: View's lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        visualize()
        setupRX()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localize()
    }
    
    private func registerCell() {
        styleCell.registerCell(tableView)
    }
    /// Class's private properties.
}

public extension FutaReasonVC {
    static func show(on vc: UIViewController,
                     styleController: FutaReasonVCStyle,
                     cell: FutaRegisterCell<C>,
                     styleTable: FutaTableViewStyle = .init(),
                     buttonClose: FutaReasonButtonCloseStyle,
                     buttonConfirm: FutaReasonButtonConfirmStyle,
                     styleInputText: FutaReasonInputTextStyle = .init(),
                     reasonInputImageProtocol: ReasonInputImageProtocol? = nil,
                     source: Observable<[Element]>) -> Observable<ReasonProtocol?> {
        return .create { (s) -> Disposable in
            let reasonVC = self.init(styleController: styleController,
                                     cell: cell,
                                     styleTable: styleTable,
                                     buttonClose: buttonClose,
                                     buttonConfirm: buttonConfirm,
                                     styleInputText: styleInputText,
                                     reasonInputImageProtocol: reasonInputImageProtocol,
                                     source: source)
            
            let dispose = reasonVC.$selected.take(1).bind { (item) in
                reasonVC.view.endEditing(true)
                reasonVC.dismiss(animated: true) {
                    s.onNext(item)
                    s.onCompleted()
                }
            }
            
            let naviVC = UINavigationController(rootViewController: reasonVC)
            naviVC.modalTransitionStyle = .coverVertical
            naviVC.modalPresentationStyle = .fullScreen
            vc.present(naviVC, animated: true, completion: nil)
            return Disposables.create(with: dispose.dispose)
        }
    }
}

// MARK: Class's private methods
private extension FutaReasonVC {
    func localize() {
        // todo: Localize view's here.
    }
    
    func visualize() {
        // todo: Visualize view's here.
        view.backgroundColor = .white
        view.clipsToBounds = true
        styleButtonConfirm.apply(btnConfirm)
        styleInputText.apply(textView)
        styleButtonClose.apply(btnClose)
        containerInput.clipsToBounds = true
        
        textView >>> {
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.isHidden = true
            $0.snp.makeConstraints { (make) in
                make.height.equalTo(styleInputText.heightInputText)
            }
        }
        
        let views: [UIView]
        if let reasonInputImageProtocol = reasonInputImageProtocol {
            views = [textView, reasonInputImageProtocol.containerView]
        } else {
            views = [textView]
        }
        
        let stackView = UIStackView(arrangedSubviews: views)
        stackView >>> containerInput >>> {
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.distribution = .fill
            $0.axis = .vertical
            $0.spacing = 16
            $0.snp.makeConstraints { (make) in
                make.edges.equalTo(styleInputText.insetContainer).priority(.high)
            }
        }
    
        btnConfirm >>> view >>> {
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.snp.makeConstraints { (make) in
                make.left.equalTo(16)
                make.right.equalTo(-16)
                make.bottom.equalTo(-30)
                make.height.equalTo(styleButtonConfirm.height)
            }
        }
        
        tableView >>> view >>> {
            $0.snp.makeConstraints { (make) in
                make.top.left.right.equalToSuperview()
                make.bottom.equalTo(btnConfirm.snp.top).offset(-16)
            }
        }
        
        btnClose >>> {
            $0.snp.makeConstraints { (make) in
                make.size.equalTo(styleButtonClose.size)
            }
        }
        
        let leftBar = UIBarButtonItem(customView: btnClose)
        navigationItem.leftBarButtonItem = leftBar
        styleController.customize(self)
        
        tableView.keyboardDismissMode = .onDrag
    }
    
    func calculateFooterTableView() {
        let size: CGSize
        let type: ReasonInputType = textView.isHidden ? .end : .begin
        if let s = cachedContainerInputSize[type] {
            size = s
        } else {
            let s = containerInput.systemLayoutSizeFitting(CGSize(width: UIScreen.main.bounds.width, height: .infinity),
                                                              withHorizontalFittingPriority: .required,
                                                              verticalFittingPriority: .fittingSizeLevel)
            cachedContainerInputSize[type] = s
            size = s
        }
        
        var f = containerInput.frame
        f += { $0.size = size.height.isFinite ? size : CGSize(width: UIScreen.main.bounds.width, height: 0) }
        containerInput.frame = f
        // Update
        tableView.tableFooterView = containerInput
    }
    
    func setupRX() {
        source.take(1).bind(to: $mSource).disposed(by: disposeBag)
        $mSource.bind(to: tableView.rx.items(cellIdentifier: C.identifier, cellType: C.self)) { [unowned self](row, element, cell) in
            cell.setupDisplay(item: element)
            self.styleCell.adjustCell?((row, element, cell))
        }.disposed(by: disposeBag)
        
        btnConfirm.rx.tap.bind(onNext: weakify({ (wSelf) in
            guard let i = wSelf.currentSelected,
                  let item = wSelf.mSource[safe: i.item] else {
                wSelf.selected = nil
                return
            }
            
            if item.allowEditDescription {
                let custom = ReasonCustom(id: item.id, description: wSelf.textView.text.orNil(""))
                wSelf.selected = custom
            } else {
                wSelf.selected = item
            }
            
        })).disposed(by: disposeBag)
        
        btnClose.rx.tap.bind(onNext: weakify({ (wSelf) in
            wSelf.selected = nil
        })).disposed(by: disposeBag)
        
        textView.rx.didBeginEditing.bind(onNext: weakify({ (wSelf) in
            wSelf.textView.typingAttributes = wSelf.styleInputText.attributeText
        })).disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .bind(to: $currentSelected)
            .disposed(by: disposeBag)
        let validImage = reasonInputImageProtocol?.valid ?? .just(true)
        
        Observable.combineLatest($currentSelected, textView.rx.text, validImage).bind(onNext: weakify({ (i, wSelf) in
            if i.0 == nil {
                wSelf.btnConfirm.isEnabled = false
            }
            guard let index = i.0, let item = wSelf.mSource[safe: index.item]  else { return }
            defer {
                if !i.2 {
                    wSelf.btnConfirm.isEnabled = false
                }
            }
            if item.allowEditDescription {
                wSelf.btnConfirm.isEnabled = !wSelf.textView.text.orNil("").isEmpty
                guard wSelf.textView.isHidden else {
                    return
                }
                wSelf.textView.isHidden = false
                wSelf.calculateFooterTableView()
                DispatchQueue.main.async {
                    wSelf.textView.becomeFirstResponder()
                }
            } else {
                wSelf.btnConfirm.isEnabled = true
                guard wSelf.tableView.isHidden == false else {
                    return
                }
                wSelf.textView.text = ""
                wSelf.textView.isHidden = true
                wSelf.calculateFooterTableView()
                wSelf.textView.resignFirstResponder()
            }
        })).disposed(by: disposeBag)
        
        let reactive = NotificationCenter.default.rx
        let eShowKeyBoard = reactive.notification(UIResponder.keyboardWillShowNotification)
        let eHideKeyBoard = reactive.notification(UIResponder.keyboardWillHideNotification)
        
        Observable.merge(eShowKeyBoard, eHideKeyBoard)
            .map(KeyboardInfo.init)
            .filterNil()
            .bind(onNext: weakify({ (info, wSelf) in
            UIView.animate(withDuration: info.duration,
                           delay: 0,
                           options: [.curveEaseInOut]) {
                wSelf.btnConfirm.snp.updateConstraints { (make) in
                    make.bottom.equalTo(-30 - info.height)
                }
                wSelf.view.layoutIfNeeded()
            } completion: { (completed) in
                guard completed, info.height > 0 else { return }
                let rect: CGRect
                if let r = wSelf.cachedRectTextView {
                    rect = r
                } else {
                    rect = wSelf.textView.convert(wSelf.textView.frame, to: wSelf.tableView)
                    wSelf.cachedRectTextView = rect
                }
                wSelf.tableView.scrollRectToVisible(rect, animated: true)
            }
        })).disposed(by: disposeBag)
    }
}
