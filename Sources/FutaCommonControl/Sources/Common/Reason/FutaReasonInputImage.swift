//  File name   : FutaReasonInputImage.swift
//
//  Author      : Dung Vu
//  Created date: 3/12/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import FwiCore
import RxSwift
import FwiCoreRX
import SnapKit
import RxCocoa

// MARK: - Protocol
public protocol ReasonInputImageProtocol {
    var containerView: UIView { get }
    var valid: Observable<Bool> { get }
    func requestUpload() -> Observable<[URL]>
}

public protocol ReasonInputImageDisplayCell: UpdateDisplayProtocol where Value == UIImage {
    var eventDelete: Observable<Void> { get }
}

public enum ReasonImageRequestAction {
    case photo
    case camera
    case none
    
    var priority: Int {
        switch self {
        case .photo:
            return 1
        case .camera:
            return 2
        case .none:
            return 3
        }
    }
}

public struct ReasonRequestOptionImage: CustomStringConvertible, Comparable {
    public let description: String
    let type: ReasonImageRequestAction
    var soureType: UIImagePickerController.SourceType? {
        switch type {
        case .camera:
            return .camera
        case .photo:
            return .photoLibrary
        default:
            return nil
        }
    }
    
    var actionType: UIAlertAction.Style {
        switch type {
        case .none:
            return .destructive
        default:
            return .default
        }
    }
    
    public init(type: ReasonImageRequestAction, description: String) {
        self.type = type
        self.description = description
    }
    
    public static func < (lhs: ReasonRequestOptionImage, rhs: ReasonRequestOptionImage) -> Bool {
        return lhs.type.priority < rhs.type.priority
    }
}

// MARK: - Implement
public final class ReasonRequestInputImage<Cell: UICollectionViewCell>: ReasonInputImageProtocol, Weakifiable, DisposableProtocol where Cell: ReasonInputImageDisplayCell
{
    private let maxItem: Int
    private let spacingItem: CGFloat
    private let sizeItem: CGSize
    
    private let minimumRequire: Int
    private let peformUpload: BlockEvent<[UIImage], [URL]>
    private let backgroundColor: UIColor
    private let typeRegister: RegisterCellType
    private let configureCell: BlockAction<(Cell, UIImage?)>?
    private let actions: [ReasonRequestOptionImage]
    private let handlerErrorImage: BlockAction<FutaPickerImageError>?
    private let description: NSAttributedString?
    @VariableReplay(wrappedValue: []) private var listImages: [UIImage]
    
    private lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = sizeItem
        flowLayout.minimumInteritemSpacing = spacingItem
        
        let result = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        result.backgroundColor = .clear
        return result
    }()
    
    public private(set) var disposeBag: DisposeBag = DisposeBag()
    public lazy var containerView: UIView = .init(frame: .zero)
    private lazy var lblDescription: UILabel = .init(frame: .zero)
    public var valid: Observable<Bool> {
        return $listImages.map(weakify({ (images, wSelf) -> Bool in
            images.count > wSelf.minimumRequire
        }, error: { () -> Error in
            fatalError("Check")
        }))
    }
    
    public init(maxItem: Int = 3,
                minimumRequire: Int = 0,
                spacingItem: CGFloat = 5,
                sizeItem: CGSize,
                description: NSAttributedString?,
                backgroundColor: UIColor = .white,
                registerCellType: RegisterCellType = .class,
                configureCell: BlockAction<(Cell, UIImage?)>? = nil,
                handlerErrorImage: BlockAction<FutaPickerImageError>? = nil,
                actionChooseImages: ReasonRequestOptionImage...,
                peformUpload: @escaping BlockEvent<[UIImage], [URL]>) {
        
        self.maxItem = maxItem
        self.spacingItem = spacingItem
        self.peformUpload = peformUpload
        self.minimumRequire = minimumRequire
        self.backgroundColor = backgroundColor
        self.sizeItem = sizeItem
        self.actions = actionChooseImages
        self.typeRegister = registerCellType
        self.configureCell = configureCell
        self.handlerErrorImage = handlerErrorImage
        self.description = description
        
        visualize()
        register()
        setupRX()
    }
    
    private func register() {
        collectionView.register(type: Cell.self, kind: typeRegister)
    }
    
    public func requestUpload() -> Observable<[URL]> {
        let images = listImages
        return peformUpload(images)
    }
}

private extension ReasonRequestInputImage {
    func visualize() {
        containerView.backgroundColor = backgroundColor
        containerView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        containerView.setContentHuggingPriority(.required, for: .vertical)
        
        lblDescription >>> containerView >>> {
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.attributedText = self.description
            $0.snp.makeConstraints { (make) in
                make.top.left.right.equalToSuperview()
            }
        }
        
        collectionView >>> containerView >>> {
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(lblDescription.snp.bottom).offset(3).priority(.high)
                make.left.right.bottom.equalToSuperview().priority(.high)
                make.height.equalTo(sizeItem.height + 2)
            }
        }
    }
    
    func configureDelete(cell: Cell, row: Int) {
        cell.eventDelete
            .takeUntil(cell.rx.methodInvoked(#selector(Cell.prepareForReuse)))
            .bind(onNext: weakify({ (wSelf) in
                var current = wSelf.listImages
                guard current[safe: row] != nil else {
                    return
                }
                current.remove(at: row)
                wSelf.listImages = current
            })).disposedByDisposable(self)
    }
    
    func requestTypeChooseImage() -> Observable<UIImagePickerController.SourceType?> {
        return .create { [unowned self] (s) -> Disposable in
            guard let topVC = UIApplication.topViewController() else {
                return Disposables.create()
            }
            
            let completed: (ReasonRequestOptionImage) -> () = { t in
                s.onNext(t.soureType)
                s.onCompleted()
            }
            
            let alertActions = self.actions.sorted(by: <).map { (r) -> UIAlertAction in
                .init(title: r.description, style: r.actionType) { (_) in
                    completed(r)
                }
            }
            
            let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertActions.forEach(alertVC.addAction(_:))
            topVC.present(alertVC, animated: true, completion: nil)
            return Disposables.create()
        }
    }
    
    func requestImage(type: UIImagePickerController.SourceType) -> Observable<FutaImagePickerResult> {
        let topVC = UIApplication.topViewController()
        let picker = FutaPickerImage(with: topVC)
        picker.showPicker(source: type, allowEdit: true)
        return picker.image.take(1)
    }
    
    func handlerImage(at index: IndexPath) {
        requestTypeChooseImage()
            .filterNil()
            .flatMap(weakify({ (type, wSelf) -> Observable<FutaImagePickerResult> in
            return wSelf.requestImage(type: type)
        }, error: { () -> Error in
            fatalError("Please Implement!!!")
        })).bind(onNext: weakify({ (result, wSelf) in
            switch result {
            case .success(let image):
                var current = wSelf.listImages
                if current[safe: index.item] != nil {
                    // Change
                    current[index.item] = image
                } else {
                    current.append(image)
                }
                wSelf.listImages = current
            case .failure(let e):
                wSelf.handlerErrorImage?(e)
            }
        })).disposedByDisposable(self)
    }
    
    func setupRX() {
        let event = $listImages.map(weakify({ (images, wSelf) -> [UIImage?] in
            let current = images
            var news: [UIImage?] = []
            news += images
            if current.count + 1 < wSelf.maxItem {
                news.append(nil)
            }
            return news
        }, error: { () -> Error in
            fatalError("Check")
        }))
        
        event.bind(to: collectionView.rx.items(cellIdentifier: Cell.identifier,
                                               cellType: Cell.self)) { [unowned self] row, item, cell in
            cell.setupDisplay(item: item)
            self.configureDelete(cell: cell, row: row)
            self.configureCell?((cell, item))
        }.disposedByDisposable(self)
        
        collectionView.rx.itemSelected.bind(onNext: weakify({ (index, wSelf) in
            wSelf.handlerImage(at: index)
        })).disposedByDisposable(self)
    }
}
