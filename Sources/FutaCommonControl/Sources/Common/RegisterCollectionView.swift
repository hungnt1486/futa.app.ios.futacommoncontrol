//  File name   : RegisterCollectionView.swift
//
//  Author      : Dung Vu
//  Created date: 3/12/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import UIKit

public enum RegisterCellType {
    case nib
    case `class`
    case unknown
}

public extension UITableView  {
    func register<Cell>(type: Cell.Type, kind: RegisterCellType) where Cell: UITableViewCell {
        switch kind {
        case .nib:
            self.register(type.nib, forCellReuseIdentifier: type.identifier)
        case .class:
            self.register(type, forCellReuseIdentifier: type.identifier)
        case .unknown:
            let bundle = Bundle(for: type)
            if bundle.url(forResource: "\(type)", withExtension: "nib") != nil {
                let nib = UINib(nibName: "\(type)", bundle: bundle)
                self.register(nib, forCellReuseIdentifier: type.identifier)
            } else {
                self.register(type, forCellReuseIdentifier: type.identifier)
            }
        }
    }
}

public extension UICollectionView  {
    func register<Cell>(type: Cell.Type, kind: RegisterCellType) where Cell: UICollectionViewCell {
        switch kind {
        case .nib:
            self.register(type.nib, forCellWithReuseIdentifier: type.identifier)
        case .class:
            self.register(type, forCellWithReuseIdentifier: type.identifier)
        case .unknown:
            let bundle = Bundle(for: type)
            if bundle.url(forResource: "\(type)", withExtension: "nib") != nil {
                let nib = UINib(nibName: "\(type)", bundle: bundle)
                self.register(nib, forCellWithReuseIdentifier: type.identifier)
            } else {
                self.register(type, forCellWithReuseIdentifier: type.identifier)
            }
        }
    }
}

public protocol CellRegister {
    associatedtype RootView
    static func registerTo(_ view: RootView, type: RegisterCellType)
}

extension UITableViewCell: CellRegister {
    public typealias RootView = UITableView
    public static func registerTo(_ view: UITableView, type: RegisterCellType) {
        view.register(type: self, kind: type)
    }
}

extension UICollectionViewCell: CellRegister {
    public typealias RootView = UICollectionView
    public static func registerTo(_ view: UICollectionView, type: RegisterCellType) {
        view.register(type: self, kind: type)
    }
}


