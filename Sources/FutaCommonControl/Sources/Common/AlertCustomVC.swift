//  File name   : AlertCustomVC.swift
//
//  Author      : Dung Vu
//  Created date: 12/19/19
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2019 Vato. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import FwiCore

// MARK: - Options
public struct OptionSetIterator<Element: OptionSet>: IteratorProtocol where Element.RawValue == Int {
    private let value: Element

    public init(element: Element) {
        self.value = element
    }

    private lazy var remainingBits = value.rawValue
    private var bitMask = 1

    public mutating func next() -> Element? {
        while remainingBits != 0 {
            defer { bitMask = bitMask &* 2 }
            if remainingBits & bitMask != 0 {
                remainingBits = remainingBits & ~bitMask
                return Element(rawValue: bitMask)
            }
        }
        return nil
    }
}

public extension OptionSet where Self.RawValue == Int {
   func makeIterator() -> OptionSetIterator<Self> {
      return OptionSetIterator(element: self)
   }
}

public struct AlertCustomOption: OptionSet, Hashable, Sequence  {
    public let rawValue: Int
    
    public static let image: AlertCustomOption = AlertCustomOption(rawValue: 1 << 0) // Option image_name
    public static let title: AlertCustomOption = AlertCustomOption(rawValue: 1 << 1)// Option AlertStyleText
    public static let customView: AlertCustomOption = AlertCustomOption(rawValue: 1 << 2)
    public static let message: AlertCustomOption = AlertCustomOption(rawValue: 1 << 3) // Option AlertStyleText
    
    public static let all: AlertCustomOption = [.image, .title, .message]
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
}

// MARK: - Styles
public protocol AlertApplyStyleProtocol {
    func apply(view: UIView)
}

extension UIView: AlertApplyStyleProtocol {
    public func apply(view: UIView) {}
}

// MARK: -- Label
public struct AlertStyleText: AlertApplyStyleProtocol {
    public let color: UIColor
    public let font: UIFont
    public let numberLines: Int
    public let textAlignment: NSTextAlignment
    
    public static let titleDefault = AlertStyleText(color: #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1), font: .systemFont(ofSize: 18, weight: .medium), numberLines: 0, textAlignment: .center)
    public static let messageDefault = AlertStyleText(color: #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1), font: .systemFont(ofSize: 15, weight: .regular), numberLines: 0, textAlignment: .center)
    public func apply(view: UIView) {
        guard let label = view as? UILabel else {
            assert(false, "Check!!!!")
            return
        }
        label.textColor = color
        label.font = font
        label.numberOfLines = numberLines
        label.textAlignment = textAlignment
    }
    
    public init(color: UIColor, font: UIFont, numberLines: Int, textAlignment: NSTextAlignment) {
        self.color = color
        self.font = font
        self.numberLines = numberLines
        self.textAlignment = textAlignment
    }
}

public struct AlertLabelValue: AlertApplyStyleProtocol {
    public let text: String?
    public let style: AlertStyleText
    public func apply(view: UIView) {
        guard let label = view as? UILabel else {
            assert(false, "Check!!!!")
            return
        }
        style.apply(view: label)
        label.text = text
    }
    
    public init(text: String?, style: AlertStyleText) {
        self.text = text
        self.style = style
    }
}

public struct AlertAttributeTextValue: AlertApplyStyleProtocol {
    public let attributedText: NSAttributedString
    public var numberOfLines: Int
    public func apply(view: UIView) {
        guard let label = view as? UILabel else {
            assert(false, "Check!!!!")
            return
        }
        label.attributedText = attributedText
        label.numberOfLines = numberOfLines
    }
    
    public init(attributedText: NSAttributedString, numberOfLines: Int = 0) {
        self.attributedText = attributedText
        self.numberOfLines = numberOfLines
    }
}

// MARK: -- Image
public struct AlertImageStyle: AlertApplyStyleProtocol {
    public let contentMode: UIView.ContentMode
    public let size: CGSize
    
    public func apply(view: UIView) {
        guard let imgView = view as? UIImageView else {
            assert(false, "Check!!!!")
            return
        }
        imgView.contentMode = contentMode
        imgView.snp.makeConstraints { (make) in
            make.size.equalTo(size)
        }
    }
    
    public init(contentMode: UIView.ContentMode, size: CGSize) {
        self.contentMode = contentMode
        self.size = size
    }
}

public struct AlertImageValue: AlertApplyStyleProtocol {
    public let imageName: String?
    public let style: AlertImageStyle
    
    public func apply(view: UIView) {
        guard let imgView = view as? UIImageView else {
            assert(false, "Check!!!!")
            return
        }
        style.apply(view: imgView)
        imgView.image = UIImage(named: imageName ?? "")
    }
    
    public init(imageName: String?, style: AlertImageStyle) {
        self.imageName = imageName
        self.style = style
    }
}

// MARK: - Alert
public typealias AlertArguments = [AlertCustomOption: AlertApplyStyleProtocol]

public final class AlertCustomVC: UIViewController {
    /// Class's public properties.
    struct Configs {
        static let paddingX: CGFloat = 48
        static let spacing: CGFloat = 20
        static let hButton: CGFloat = 48
        static let spaceButton: CGFloat = 0
    }
    
    private var buttons: [AlertActionProtocol]
    private let option: AlertCustomOption
    private let orderType: NSLayoutConstraint.Axis
    private let alignment: UIStackView.Alignment
    private var containerView: UIView?
    private lazy var disposeBag = DisposeBag()
    private let arguments: AlertArguments
    private let config: AlertConfigCustomViewProtocol
    private let speratorColor: UIColor
    private let edges: UIEdgeInsets
    
    public init(with option: AlertCustomOption,
         arguments: [AlertCustomOption: AlertApplyStyleProtocol],
         config: AlertConfigCustomViewProtocol,
         buttons: [AlertActionProtocol],
         edges: UIEdgeInsets,
         speratorColor: UIColor,
         orderType: NSLayoutConstraint.Axis,
         alignment: UIStackView.Alignment)
    {
        self.config = config
        self.arguments = arguments
        self.orderType = orderType
        self.buttons = buttons
        self.speratorColor = speratorColor
        self.option = option
        self.alignment = alignment
        self.edges = edges
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View's lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        visualize()
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localize()
        config.startAnimation(contentView: containerView)
    }

    /// Class's private properties.
}

// MARK: View's event handlers
public extension AlertCustomVC {
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

// MARK: Class's private methods
private extension AlertCustomVC {
    private func localize() {
        // todo: Localize view's here.
    }
    private func visualize() {
        // todo: Visualize view's here.
        config.customBackground?(view)
        // MARK: - Container
        let containerView = UIView(frame: .zero)
        containerView >>> view >>> {
            $0.backgroundColor = .white
            $0.layer.cornerRadius = 12
            $0.clipsToBounds = true
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.snp.makeConstraints { (make) in
                make.width.equalTo(UIScreen.main.bounds.width - Configs.paddingX)
                make.center.equalToSuperview()
            }
        }
        self.containerView = containerView
        
        // MARK: - Check
        var childViews: [UIView] = []
        
        // Custom View
        option.forEach { (o) in
            switch o {
            case .customView:
                guard let customView = arguments[.customView] as? UIView else {
                    assert(false, "Check !!!")
                    return
                }
                childViews.append(customView)
            case .image:
                guard let style = arguments[.image] else {
                    assert(false, "Check !!!")
                    return
                }
                let imageView = UIImageView(frame: .zero)
                style.apply(view: imageView)
                childViews.append(imageView)
            case .title:
                guard let style = arguments[.title]  else {
                    assert(false, "Check !!!")
                    return
                }
                let label = UILabel(frame: .zero)
                label.setContentHuggingPriority(.defaultLow, for: .horizontal)
                style.apply(view: label)
                childViews.append(label)
            case .message:
                guard let style = arguments[.message] else {
                    assert(false, "Check !!!")
                    return
                }
                let label = UILabel(frame: .zero)
                style.apply(view: label)
                label.setContentHuggingPriority(.defaultLow, for: .horizontal)
                childViews.append(label)
            default:
                fatalError("Please Implement")
            }
        }
        
        // MARK: - Content
        assert(!childViews.isEmpty, "Check !!!")
        
        let stackView = UIStackView(arrangedSubviews: childViews)
        stackView >>> containerView >>> {
            $0.spacing = 20
            $0.distribution = .fill
            $0.alignment = alignment
            $0.axis = .vertical
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(edges.top)
                make.left.equalTo(edges.left)
                make.right.equalTo(-edges.right)
            }
        }
        
        let bottomView = UIView(frame: .zero)
        
        let height: CGFloat
        switch self.orderType {
        case .horizontal:
            height = Configs.hButton
        case .vertical:
            let number = CGFloat(self.buttons.count)
            height = Configs.hButton * number + (number - 1) * Configs.spaceButton
        @unknown default:
            fatalError("Please Implement!!!")
        }
        
        bottomView >>> containerView >>> {
            $0.backgroundColor = .white
            $0.addSeperator(with: .zero, position: .top)
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(stackView.snp.bottom).offset(20)
                make.left.right.equalToSuperview()
                make.height.equalTo(height)
                make.bottom.equalTo(-edges.bottom).priority(.high)
            }
        }
        
        let style: SeperatorPositon = orderType == .horizontal ? .right : .bottom
        let count = self.buttons.count
        
        let buttons = self.buttons.enumerated().map { (idx, action) -> UIButton in
            let b = UIButton(frame: .zero)
            action.apply(button: b)
            
            action.invokedDismissMethod.bind(onNext: weakify({ (wSelf) in
                wSelf.dismiss(animated: true, completion: nil)
            })).disposed(by: self.disposeBag)
            
            b.rx.tap.bind { [weak self] in
                guard action.autoDismiss else {
                    return action.handler()
                }
                self?.dismiss(animated: true, completion: action.handler)
            }.disposed(by: self.disposeBag)
            if idx < count - 1 {
                b.addSeperator(with: .zero, position: style, color: speratorColor)
            }
            return b
        }

        
        UIStackView(arrangedSubviews: buttons) >>> {
            $0.axis = self.orderType
            $0.distribution = .fillEqually
            $0.spacing = Configs.spaceButton
        } >>> bottomView >>> {
            $0.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
                
            }
        }
        bottomView.addSeperator(with: .zero, position: .top, color: speratorColor)
        config.beginAnimation(contentView: containerView)
        self.containerView = containerView
    }
}

// MARK: -- Config Animation
public protocol AlertConfigCustomViewProtocol {
    var customBackground: BlockAction<UIView>? { get }
    func beginAnimation(contentView: UIView?)
    func startAnimation(contentView: UIView?)
}

public struct AlertConfigAnimationDefault: AlertConfigCustomViewProtocol {
    public static let `default` = AlertConfigAnimationDefault()
    public let customBackground: BlockAction<UIView>? = {
        $0.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 0.4)
    }

    public func beginAnimation(contentView: UIView?) {
        contentView?.transform = CGAffineTransform(translationX: 0, y: 1000)
    }

    public func startAnimation(contentView: UIView?) {
        UIView.animate(withDuration: 0.5) {
            contentView?.transform = .identity
        }
    }
}

public final class AlertConfettiAnimationDefault: AlertConfigCustomViewProtocol {
    public static let `default` = AlertConfettiAnimationDefault()
    private lazy var confetti: ConfettiView = ConfettiView()
    public let customBackground: BlockAction<UIView>? = { view in
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        let containerView = UIView(frame: .zero)
        containerView.backgroundColor = .clear
        blurView >>> containerView >>> {
            $0.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
        containerView >>> view >>> {
            $0.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
    }
    
    public func beginAnimation(contentView: UIView?) {
        contentView?.alpha = 0
        contentView?.transform = CGAffineTransform(translationX: 0, y: -1000)
        guard let s = contentView?.superview else { return }
        confetti >>> s >>> {
            $0.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
    }
    
    public func startAnimation(contentView: UIView?) {
        UIView.animate(withDuration: 0.5, delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 5,
                       options: .curveEaseInOut,
                       animations: {
            contentView?.alpha = 1
            contentView?.transform = .identity
        }) { [unowned self](_) in
            self.confetti.emit(with: [.shape(.square, #colorLiteral(red: 0.3568627451, green: 0.8705882353, blue: 1, alpha: 1), CGSize(width: 2, height: 4)),
                                      .shape(.square, #colorLiteral(red: 1, green: 0.6274509804, blue: 0.6274509804, alpha: 1), CGSize(width: 2, height: 4)),
                                      .shape(.square, #colorLiteral(red: 0.768627451, green: 0.8666666667, blue: 0.5843137255, alpha: 1), CGSize(width: 2, height: 4)),
                                      .shape(.square, #colorLiteral(red: 1, green: 0.6745098039, blue: 0.4784313725, alpha: 1), CGSize(width: 2, height: 4)),
                                      .shape(.square, #colorLiteral(red: 0.9254901961, green: 0.4784313725, blue: 1, alpha: 1), CGSize(width: 2, height: 4))])
        }
    }
}

// MARK: -- Protocol Action
public protocol AlertActionProtocol {
    var handler: AlertBlock { get }
    var autoDismiss: Bool { get }
    var invokedDismissMethod: Observable<Void> { get }
    func apply(button: UIButton)
}

public extension AlertActionProtocol {
    var autoDismiss: Bool {
        return true
    }
}

// MARK: -- Show
public extension AlertCustomVC {
    static func show(on vc: UIViewController?,
                     option: AlertCustomOption,
                     arguments: AlertArguments,
                     config: AlertConfigCustomViewProtocol = AlertConfigAnimationDefault.default,
                     buttons: [AlertActionProtocol],
                     edges: UIEdgeInsets = .init(top: 20, left: 20, bottom: 0, right: 20),
                     speratorColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1),
                     orderType: NSLayoutConstraint.Axis,
                     alignment: UIStackView.Alignment = .center)
    {
        guard let vc = vc else {
            assert(false, "Check")
            return
        }
        
        let alertVC = AlertCustomVC(with: option,
                                    arguments: arguments,
                                    config: config,
                                    buttons: buttons,
                                    edges: edges,
                                    speratorColor: speratorColor,
                                    orderType: orderType,
                                    alignment: alignment)
        alertVC.modalTransitionStyle = .crossDissolve
        alertVC.modalPresentationStyle = .overCurrentContext
        vc.present(alertVC, animated: true, completion: nil)
    }
}

