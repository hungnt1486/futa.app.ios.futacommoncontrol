//  File name   : Disposeable + Extension.swift
//
//  Author      : Dung Vu
//  Created date: 3/4/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import FwiCoreRX
import RxSwift

public extension DisposableProtocol {
    
    /// Handler all dipose in group
    /// - Parameter disposables: list dispose need to register
    func register(disposables: Disposable...) {
        disposeBag.insert(disposables)
    }
}

public extension Disposable {
    
    /// Help add to diposeable protocol
    /// - Parameter handler: conform `DisposableProtocol` to help
    func disposedByDisposable(_ handler: DisposableProtocol) {
        handler.register(disposables: self)
    }
}


