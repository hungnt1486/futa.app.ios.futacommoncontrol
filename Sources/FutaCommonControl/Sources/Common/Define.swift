//  File name   : Define.swift
//
//  Author      : Dung Vu
//  Created date: 2/26/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  
//  --------------------------------------------------------------

import UIKit
import RxSwift

public typealias BlockAction<T> = (T) -> ()
public typealias AlertBlock = () -> Void
public typealias BlockEvent<T, E> = (T) -> Observable<E>

public extension UIView {
    var edgeSafe: UIEdgeInsets {
        if #available(iOS 11, *) {
            return self.safeAreaInsets
        }
        return UIEdgeInsets.topEdge(20)
    }
}
