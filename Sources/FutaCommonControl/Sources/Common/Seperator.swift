//  File name   : Seperator.swift
//
//  Author      : Dung Vu
//  Created date: 2/26/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import FwiCore
import SnapKit

public protocol CreateViewCodeProtocol {}
public extension CreateViewCodeProtocol where Self: UIView {
    static func create(_ transform: (Self) -> ()) -> Self {
        let v = self.init(frame: .zero)
        transform(v)
        return v
    }
}

extension UIView: CreateViewCodeProtocol {}

public enum SeperatorPositon {
    case top
    case bottom
    case right
    case left
}

public extension UIView {
    static var nib: UINib? {
        let name = "\(self)"
        return UINib(nibName: name, bundle: nil)
    }
    
    @discardableResult
    func addSeperator(with edges: UIEdgeInsets = .zero,
                      position: SeperatorPositon = .bottom,
                      color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)) -> UIView
    {
        let s = UIView.create {
            $0.backgroundColor = color
        }
        
        switch position {
        case .top:
            s >>> self >>> {
                $0.snp.makeConstraints({ (make) in
                    make.height.equalTo(0.5)
                    make.left.equalTo(edges.left)
                    make.right.equalTo(-edges.right).priority(.low)
                    make.top.equalToSuperview()
                })
            }
        case .bottom:
            s >>> self >>> {
                $0.snp.makeConstraints({ (make) in
                    make.height.equalTo(0.5)
                    make.left.equalTo(edges.left)
                    make.right.equalTo(-edges.right).priority(.low)
                    make.bottom.equalTo(-edges.bottom)
                })
            }
        case .right:
            s >>> self >>> {
                $0.snp.makeConstraints({ (make) in
                    make.width.equalTo(0.5)
                    make.top.bottom.equalToSuperview()
                    make.right.equalTo(-edges.right)
                })
            }
        case .left:
            s >>> self >>> {
                $0.snp.makeConstraints({ (make) in
                    make.width.equalTo(0.5)
                    make.top.bottom.equalToSuperview()
                    make.left.equalTo(edges.left)
                })
            }
        }
        return s
    }
    
    func borderView(with color: UIColor, width: CGFloat, andRadius radius: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
