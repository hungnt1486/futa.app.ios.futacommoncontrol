//  File name   : DecoderModel.swift
//
//  Author      : Dung Vu
//  Created date: 9/8/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Vato. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public extension Dictionary {
    func convertToJSON() throws -> Data {
        return try JSONSerialization.data(withJSONObject: self, options: [])
    }
}

public extension Dictionary {
    func value<E>(for key: Key, defaultValue: @autoclosure () -> E) -> E {
        guard let result = self[key] as? E else {
            return defaultValue()
        }
        return result
    }
    
    subscript(from keys: Key...) -> [Value] {
        var result = [Value]()
        keys.forEach { (k) in
            guard let v = self[k] else {
                return
            }
            result.append(v)
        }
        return result
    }
    
    subscript(optional key: Key?) -> Value? {
        set {
            guard let key = key else { return }
            self[key] = newValue
        }
        
        get {
            guard let key = key else { return nil }
            return self[key]
        }
    }
    
    subscript<E>(_ key: Key, defaultValue: @autoclosure () -> E) -> E {
        return value(for: key, defaultValue: defaultValue())
    }
    
    static func +=(lhs: inout Self, rhs: Self?) {
        rhs?.forEach({ (element) in
            lhs[element.key] = element.value
        })
    }
}

public extension Array {
    func convertToData() throws -> Data {
        return try JSONSerialization.data(withJSONObject: self, options: [])
    }
}

public extension Decodable {
    static func toModel(from data: Data, block: ((JSONDecoder) -> Void)? = nil) throws -> Self {
        let decoder = JSONDecoder()
        let d = data
        // custom
        block?(decoder)
        do {
            let result = try decoder.decode(self, from: d)
            return result
        } catch let err as NSError {
            debugPrint(err)
            throw err
        }
    }

    static func toModel(from json: [String: Any]?, block: ((JSONDecoder) -> Void)? = nil) throws -> Self {
        guard let data = try json?.toData() else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: [NSLocalizedDescriptionKey: "Not available data!!!"])
        }
        return try self.toModel(from: data, block: block)
    }
}

public extension Encodable {
    func toJSON() throws -> [String: Any] {
        let data = try toData()
        let value = try JSONSerialization.jsonObject(with: data, options: [])
        guard let json = value as? [String: Any] else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: [NSLocalizedDescriptionKey : "Failed make json!!!!"])
        }
        return json
    }
    
    func toData() throws -> Data {
        let encoder = JSONEncoder()
        let data = try encoder.encode(self)
        return data
    }
}
