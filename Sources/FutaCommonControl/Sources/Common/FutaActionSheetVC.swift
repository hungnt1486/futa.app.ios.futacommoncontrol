//  File name   : VatoActionSheetVC.swift
//
//  Author      : Dung Vu
//  Created date: 7/23/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Vato. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import FwiCore
import FwiCoreRX

open class FutaActionSheetVC<C: UITableViewCell>: UIViewController, UITableViewDelegate where C: UpdateDisplayProtocol, C.Value: Equatable {
    /// Class's public properties.
    public typealias D = C.Value
    public typealias Cell = C
    
    public private(set) lazy var tableView = UITableView(frame: .zero, style: .plain)
    private lazy var lblTitle: UILabel = UILabel(frame: .zero)
    private lazy var lblDescription: UILabel = UILabel(frame: .zero)
    private lazy var btnClose: UIButton = UIButton(frame: .zero)
    private (set) lazy var mContainerView: UIView = {
        let v = FutaHeaderCornerView(with: 8)
        v.containerColor = .white
        return v
    }()
    
    internal let source: Observable<[D]>
    private let currentSelect: Observable<D>
    @VariableReplay public private(set) var mSource: [D] = []
    @Publisher public internal(set) var selected: D?
    internal lazy var disposeBag = DisposeBag()
    private let type: RegisterCellType
    private let heightCell: CGFloat
    private let mTitle: String?
    private let mDescription: String?
    
    // MARK: Constructor
    required public init(from source: Observable<[D]>,
                         currentSelect: Observable<D>,
                         type: RegisterCellType,
                         heightCell: CGFloat,
                         title: String?,
                         description: String?) {
        self.currentSelect = currentSelect
        self.source = source
        self.type = type
        self.mTitle = title
        self.heightCell = heightCell
        self.mDescription = description
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View's lifecycle
    open override func viewDidLoad() {
        super.viewDidLoad()
        visualize()
        setupRX()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    /// Class's private properties.
    open override var prefersStatusBarHidden: Bool {
        return false
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first else {
            return
        }

        let p = point.location(in: self.view)
        guard self.mContainerView.frame.contains(p) == false else {
            return
        }
        actionSheetCancel()
    }
    
    internal func selectDefaultRow() {
        Observable.combineLatest($mSource, currentSelect, resultSelector: { ($0, $1) }).bind(onNext: weakify({ (items, wSelf) in
            guard let idx = items.0.firstIndex(of: items.1) else {
                return
            }
            wSelf.tableView.selectRow(at: IndexPath(item: idx, section: 0), animated: false, scrollPosition: .none)
        })).disposed(by: disposeBag)
    }
    
    // MARK: -- Setup Display
    open func visualize() {
        tableView.register(type: C.self, kind: type)
        self.view.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 0.4)
        tableView.separatorStyle = .none
        tableView.rowHeight = heightCell
        tableView.estimatedRowHeight = 100
        tableView.separatorColor = .clear
        
        mContainerView >>> view >>> {
            $0.snp.makeConstraints { (make) in
                make.left.right.bottom.equalToSuperview()
                make.top.equalTo(self.view.layoutMarginsGuide.snp.top)
            }
        }
        
        lblTitle >>> {
            $0.textColor = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
            $0.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            $0.text = mTitle
            $0.textAlignment = .center
            $0.numberOfLines = 2
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
        }
        
        lblDescription >>> {
            $0.textColor = #colorLiteral(red: 0.3882352941, green: 0.4470588235, blue: 0.5019607843, alpha: 1)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            $0.numberOfLines = 0
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
        }
        
        let stackView = UIStackView(arrangedSubviews: [lblTitle, lblDescription])
        stackView >>> mContainerView >>> {
            $0.distribution = .fill
            $0.axis = .vertical
            $0.spacing = 8
            $0.alignment = .center
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(24)
                make.left.equalTo(16)
                make.right.equalTo(-16)
            }
        }
        
        if let d = mDescription, !d.isEmpty {
            lblDescription.text = d
        } else {
            lblDescription.isHidden = true
        }
        
        btnClose >>> mContainerView >>> {
            $0.setImage(UIImage(named: "ic_close"), for: .normal)
            $0.snp.makeConstraints { (make) in
                make.top.right.equalToSuperview()
                make.size.equalTo(CGSize(width: 56, height: 44))
            }
        }
        
        tableView >>> mContainerView >>> {
            $0.snp.makeConstraints { (make) in
                make.left.right.bottom.equalToSuperview()
                make.top.equalTo(stackView.snp.bottom).offset(16).priority(.high)
            }
        }
        mContainerView.alpha = 0
        mContainerView.transform = CGAffineTransform(translationX: 0, y: 1500)
    }
    
    
    /// Function handler action cancel user on action sheet
    open func actionSheetCancel() {
        selected = nil
    }
    
    open func heightContainerActionSheet() -> CGFloat {
        let r = tableView.rect(forSection: 0)
        let f = tableView.frame
        let h = f.origin.y + r.height
        let y = view.bounds.height - h - 80
        return y
    }
    
    // MARK: -- Setup Event Handler
    open func setupRX() {
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        source.bind(to: $mSource).disposed(by: disposeBag)
        btnClose.rx.tap.bind(onNext: weakify({ (wSelf) in
            wSelf.actionSheetCancel()
        })).disposed(by: disposeBag)
        
        setupDisplayCell()
        
        $mSource.filter { !$0.isEmpty }.delay(.milliseconds(300), scheduler: MainScheduler.asyncInstance).bind(onNext: weakify({ (_, wSelf) in
            let y = wSelf.heightContainerActionSheet()
            let transform: CGAffineTransform = y > 0 ? CGAffineTransform(translationX: 0, y: y ) : .identity
            UIView.animate(withDuration: 0.1) {
                wSelf.mContainerView.alpha = 1
                wSelf.mContainerView.transform = transform
            }
            wSelf.selectDefaultRow()
        })).disposed(by: disposeBag)
        setupEventSelect()
    }
    
    private func setupDisplayCell() {
        $mSource.bind(to: tableView.rx.items(cellIdentifier: C.identifier, cellType: C.self)) { [unowned self] idx, element, cell in
            self.updateDisplay(index: idx, element: element, cell: cell)
        }.disposed(by: disposeBag)
    }
    
    open func updateDisplay(index: Int, element: D, cell: C) {
        cell.setupDisplay(item: element)
    }

    open func setupEventSelect() {
        tableView.rx.itemSelected.flatMap { [weak self](index) -> Observable<D?> in
            guard let wSelf = self else { return Observable.empty() }
            return wSelf.source.take(1).map { (list) -> D? in
                let item = list[safe: index.item]
                return item
            }
        }
        .filterNil()
        .bind(to: $selected)
        .disposed(by: disposeBag)
    }
    
    // MARK: -- Table Delegate
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightCell
    }
    
    open func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    open func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    open func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {}
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? { return nil }

    open func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? { return nil }
    
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { return 0.1 }

    open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { return 0.1 }
    
    // MARK: -- Display
    open class func showUse(source: Observable<[D]>,
                        on controller: UIViewController?,
                        currentSelect: Observable<D>,
                        type: RegisterCellType = .class,
                        heightCell: CGFloat = UITableView.automaticDimension,
                        title: String? = nil,
                        description: String? = nil) -> Observable<D?>
    {
        return Observable.create { (s) -> Disposable in
            let selectVC = self.init(from: source, currentSelect: currentSelect, type: type, heightCell: heightCell, title: title, description: description)
            selectVC.modalPresentationStyle = .overCurrentContext
            selectVC.modalTransitionStyle = .crossDissolve
            let disposable = selectVC.$selected.take(1).bind { (item) in
                selectVC.dismiss(animated: true) {
                    s.onNext(item)
                    s.onCompleted()
                }
            }

            controller?.present(selectVC, animated: true, completion: nil)
            return Disposables.create(with: disposable.dispose)
        }
    }
}



