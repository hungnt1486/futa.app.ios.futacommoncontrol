//  File name   : TextView.swift
//
//  Author      : Dung Vu
//  Created date: 3/11/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import SnapKit
import FwiCore

fileprivate struct TextViewPlaceHolder {
    static var placeHolder = "UITextViewPlaceHolder"
}

extension UITextView: Weakifiable {
    private func createPlaceHolder() -> UILabel {
        let inset = self.textContainerInset
        let placeholderTextView = UILabel(frame: .zero)
        addSubview(placeholderTextView)
        self.sendSubviewToBack(placeholderTextView)
        placeholderTextView.isUserInteractionEnabled = false
        placeholderTextView.snp.makeConstraints { (make) in
            make.left.equalTo(inset.left + 6)
            make.top.equalTo(inset.top)
            make.right.equalTo(-inset.right)
        }
        
        let handlerNotification: BlockAction<Notification> = weakify { [weak placeholderTextView](notify, wSelf) in
            guard (notify.object as? UITextView) === wSelf else { return }
            DispatchQueue.main.async {
                placeholderTextView?.isHidden = !wSelf.text.orNil("").isEmpty
            }
        }
        
        let center = NotificationCenter.default
        center.addObserver(forName: UITextView.textDidChangeNotification, object: nil, queue: nil, using: handlerNotification)
        center.addObserver(forName: UITextView.textDidBeginEditingNotification, object: nil, queue: nil, using: handlerNotification)
        return placeholderTextView
    }
    
    internal var placeHolderLabel: UILabel {
        get {
            guard let label = objc_getAssociatedObject(self, &TextViewPlaceHolder.placeHolder) as? UILabel else {
                let new = createPlaceHolder()
                self.placeHolderLabel = new
                return new
            }
            return label
        }
        
        set {
            objc_setAssociatedObject(self, &TextViewPlaceHolder.placeHolder, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public var placeHolderAttribute: NSAttributedString? {
        get {
            placeHolderLabel.attributedText
        }
        
        set {
            placeHolderLabel.attributedText = newValue
        }
    }
}


