//  File name   : WeakifiableProtocol.swift
//
//  Author      : Dung Vu
//  Created date: 9/8/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Vato. All rights reserved.
//  --------------------------------------------------------------

import UIKit

// MARK: - weakitify code
public protocol Weakifiable: AnyObject {}
public extension Weakifiable {
    func weakify(_ code: @escaping (Self) -> Void) -> () -> Void {
        return { [weak self] in
            guard let self = self else { return }
            code(self)
        }
    }
    
    func weakify<T>(_ code: @escaping (T, Self) -> Void) -> (T) -> Void {
        return { [weak self] arg in
            guard let self = self else { return }
            code(arg, self)
        }
    }
    
    func weakify<T, E>(_ code: @escaping (T, Self) -> E,
                       error: @escaping () -> Error) -> (T) throws -> E
    {
        return { [weak self] arg in
            guard let self = self else { throw error() }
            return code(arg, self)
        }
    }
}

// MARK: - Controller
extension UIViewController: Weakifiable {}
