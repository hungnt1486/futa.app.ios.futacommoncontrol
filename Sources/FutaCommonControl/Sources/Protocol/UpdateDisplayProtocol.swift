//  File name   : UpdateDisplayProtocol.swift
//
//  Author      : Dung Vu
//  Created date: 9/8/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Vato. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public protocol UpdateDisplayProtocol {
    associatedtype Value
    func setupDisplay(item: Value?)
}
