//  File name   : PagingNextProtocol.swift
//
//  Author      : Dung Vu
//  Created date: 2/26/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public protocol PagingNextProtocol {
    static var `default`: Self { get }
    var page: Int { get }
    var size: Int { get }
    var canRequest: Bool { get }
    var next: Self? { get }
    var first: Bool { get }
    init(page: Int, canRequest: Bool, size: Int)
}

public extension PagingNextProtocol {
    var next: Self? {
        guard canRequest else {
            return nil
        }
        return Self(page: page + 1, canRequest: false, size: size)
    }
    
    var first: Bool {
        return page <= 1
    }
}

public enum ListUpdate<T> {
    case reload(items: [T])
    case update(items: [T])
}
