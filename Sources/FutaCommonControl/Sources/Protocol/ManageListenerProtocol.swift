//  File name   : ManageListenerProtocol.swift
//
//  Author      : Dung Vu
//  Created date: 3/3/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import RxSwift

public protocol ManageListenerProtocol: AnyObject, SafeAccessProtocol {
    var listenerManager: [Disposable] { get set }
}

public extension ManageListenerProtocol {
    func cleanUpListener() {
        excute(block: { [unowned self] in
            self.listenerManager.forEach({ $0.dispose() })
            self.listenerManager.removeAll()
        })
    }
    
    func add(_ disposable: Disposable) {
        excute(block: { [unowned self] in
            self.listenerManager.append(disposable)
        })
    }
}
