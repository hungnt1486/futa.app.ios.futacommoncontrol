//  File name   : Property.swift
//
//  Author      : Dung Vu
//  Created date: 1/2/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Vato. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import RxSwift
import RxCocoa
import FwiCore

// MARK: - Safe protocol
public protocol SafeAccessProtocol {
    var lock: NSRecursiveLock { get }
}

public extension SafeAccessProtocol {
    @discardableResult
    func excute<T>(block: () -> T) -> T {
        lock.lock()
        defer { lock.unlock() }
        return block()
    }
}

// MARK: - Thread safe
@propertyWrapper
public struct ThreadSafe<T>: SafeAccessProtocol {
    public let lock: NSRecursiveLock = NSRecursiveLock()
    var _mValue: T?
    public var wrappedValue: T? {
        get {
            return excute { _mValue }
        }
        
        set {
            excute { _mValue = newValue }
        }
    }
    
    public var projectedValue: T {
        fatalError("Please Implement!!!")
    }
    
    public init() {}
}

// MARK: - Replay
@propertyWrapper
public struct Replay<T> {
    private let _event: ReplaySubject<T>
    private let queue: ImmediateSchedulerType
    public init(bufferSize: Int, queue: ImmediateSchedulerType) {
        self.queue = queue
        _event = ReplaySubject<T>.create(bufferSize: bufferSize)
    }
    
    public init(queue: ImmediateSchedulerType) {
        self.queue = queue
       _event = ReplaySubject<T>.create(bufferSize: 1)
    }
    
    public var wrappedValue: T {
        get {
            fatalError("Do not get value from this!!!!")
        }
        
        set {
            _event.onNext(newValue)
        }
    }
    
    public var projectedValue: Observable<T> {
        return _event.observeOn(queue)
    }
}

// MARK: - BehaviorReplay
@propertyWrapper
public struct VariableReplay<T> {
    private let replay: BehaviorRelay<T>
    
    public init(wrappedValue: T) {
        replay = BehaviorRelay(value: wrappedValue)
    }
    
    public var wrappedValue: T {
        get {
            return replay.value
        }
        
        set {
            replay.accept(newValue)
        }
    }
    
    public var projectedValue: BehaviorRelay<T> {
        return replay
    }
}

// MARK: - Published
@propertyWrapper
public struct Publisher<T> {
    private let subject: PublishSubject<T> = PublishSubject()
    public var wrappedValue: T {
        get {
            fatalError("Do not get value from this!!!!")
        }
        
        set {
            subject.onNext(newValue)
        }
    }
    
    public var projectedValue: PublishSubject<T> {
        return subject
    }
    
    public init() {}
}

// MARK: - Expired
@propertyWrapper
public struct Exprired<T> {
    private let timeExpired: TimeInterval
    private var date: Date
    public init(wrappedValue: T, timeExpired: TimeInterval) {
        self.wrappedValue = wrappedValue
        self.timeExpired = timeExpired
        date = Date().addingTimeInterval(timeExpired)
    }
    
    public var wrappedValue: T {
        didSet {
            date = Date().addingTimeInterval(timeExpired)
        }
    }
    
    public var projectedValue: T? {
        guard date.timeIntervalSince(Date()) > 0 else {
            return nil
        }
        
        return wrappedValue
    }
}

// MARK: - Trim
@propertyWrapper
public struct Trimmed {
    private(set) var value: String = ""

    public var wrappedValue: String {
        get { value }
        set { value = newValue.trimmingCharacters(in: .whitespacesAndNewlines) }
    }
    
    public var projectedValue: String {
        fatalError("Please Implement!!!")
    }

    public init(wrappedValue: String) {
        self.wrappedValue = wrappedValue
    }
}

// MARK: - UserDefault
@propertyWrapper
public struct UserDefault<T> {
    let key: String
    let defaultValue: T

    public init(_ key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }

    public var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
    
    public var projectedValue: T {
        fatalError("Please Implement!!!")
    }
}

// MARK: - Cache To File
@propertyWrapper
public struct CacheFile<T> where T: Codable, T: Equatable {
    public var fName: String = "" {
        didSet { load() }
    }
    @VariableReplay(wrappedValue: []) private var response: [T]
    private let cacheDocument = URL.cacheDirectory()
    
    public init(fileName: String = "") {
        self.fName = fileName
        load()
    }
    
    public var wrappedValue: [T] {
        get {
            return response
        }
        
        set {
            response = newValue
        }
    }
    
    public var projectedValue: Observable<[T]> {
        return $response.asObservable()
    }
    
    private mutating func load() {
        guard !fName.isEmpty else { return }
        let fileManager = FileManager.default
        guard let url = cacheDocument?.appendingPathComponent(fName), fileManager.fileExists(url) else {
            return
        }

        do {
            let data = try Data(contentsOf: url)
            let items = try [T].toModel(from: data)
            self.response = items
        } catch {
            assert(false, error.localizedDescription)
        }
    }
    
    public mutating func add(item: T?, clear: Bool = true) {
        guard let item = item else {
            return
        }
        
        guard !clear else {
            response = [item]
            return
        }

        var current = response
        if let idx = current.firstIndex(of: item) {
            current.remove(at: idx)
        }

        current.insert(item, at: 0)
        response = current
    }
    
    public func save() {
        guard let url = cacheDocument?.appendingPathComponent(fName) else {
            return
        }

        let items = response
        do {
            let data = try items.toData()
            try data.write(to: url)
        } catch {
            assert(false, error.localizedDescription)
        }
    }
}








