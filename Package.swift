// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FutaCommonControl",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FutaCommonControl",
            targets: ["FutaCommonControl"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://gitlab.com/hungnt1486/futa.app.ios.fwicorerx", branch: "main"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", from: "5.0.1")
        
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "FutaCommonControl",
            dependencies: [
                .product(name: "FwiCoreRX", package: "futa.app.ios.fwicorerx"),
                .product(name: "SnapKit", package: "SnapKit")
            ],
            path: "Sources"
        ),
    ]
)
